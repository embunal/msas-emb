# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "C"
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_C
  "/home/pi/Documents/msas_demo/examples/msas_demo/nfc.c" "/home/pi/Documents/msas_demo/build/examples/msas_demo/CMakeFiles/msas.dir/nfc.c.o"
  )
set(CMAKE_C_COMPILER_ID "GNU")
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/pi/Documents/msas_demo/examples/msas_demo/msas.cpp" "/home/pi/Documents/msas_demo/build/examples/msas_demo/CMakeFiles/msas.dir/msas.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS
  "DEBUG"
  "LINUX"
  "NATIVE_C_CODE"
  "NXPBUILD_CUSTOMER_HEADER_INCLUDED"
  "NXPBUILD__PHHAL_HW_RC523"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/pi/Documents/msas_demo/build/linux/CMakeFiles/NxpRdLibLinuxPN512.dir/DependInfo.cmake"
  "/home/pi/Documents/msas_demo/build/examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "../nxprdlib/NxpRdLib/intfs"
  "../nxprdlib/NxpRdLib/types"
  "../nxprdlib/NxpRdLib"
  "../linux/intfs"
  "../linux/comps/phbalReg/src/Linux"
  "../linux/shared"
  "../examples/msas_demo/intfs"
  "../nxprdlib/NxpRdLib/comps/phbalReg/src/Stub"
  "../linux/comps/phPlatform/src/Posix"
  "../linux/comps/phOsal/src/Posix"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
