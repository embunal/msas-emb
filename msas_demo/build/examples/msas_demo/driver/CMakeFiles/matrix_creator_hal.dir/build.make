# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.0

#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:

# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list

# Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:
.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/pi/Documents/msas_demo

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/pi/Documents/msas_demo/build

# Include any dependencies generated for this target.
include examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/depend.make

# Include the progress variables for this target.
include examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/progress.make

# Include the compile flags for this target's objects.
include examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o: ../examples/msas_demo/driver/matrix_driver.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/Documents/msas_demo/build/CMakeFiles $(CMAKE_PROGRESS_1)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o -c /home/pi/Documents/msas_demo/examples/msas_demo/driver/matrix_driver.cpp

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.i"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/Documents/msas_demo/examples/msas_demo/driver/matrix_driver.cpp > CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.i

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.s"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/Documents/msas_demo/examples/msas_demo/driver/matrix_driver.cpp -o CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.s

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.requires:
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.provides: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.requires
	$(MAKE) -f examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.provides.build
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.provides

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.provides.build: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o: ../examples/msas_demo/driver/everloop.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/Documents/msas_demo/build/CMakeFiles $(CMAKE_PROGRESS_2)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o -c /home/pi/Documents/msas_demo/examples/msas_demo/driver/everloop.cpp

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/matrix_creator_hal.dir/everloop.cpp.i"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/Documents/msas_demo/examples/msas_demo/driver/everloop.cpp > CMakeFiles/matrix_creator_hal.dir/everloop.cpp.i

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/matrix_creator_hal.dir/everloop.cpp.s"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/Documents/msas_demo/examples/msas_demo/driver/everloop.cpp -o CMakeFiles/matrix_creator_hal.dir/everloop.cpp.s

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.requires:
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.provides: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.requires
	$(MAKE) -f examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.provides.build
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.provides

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.provides.build: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o: ../examples/msas_demo/driver/wishbone_bus.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/Documents/msas_demo/build/CMakeFiles $(CMAKE_PROGRESS_3)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o -c /home/pi/Documents/msas_demo/examples/msas_demo/driver/wishbone_bus.cpp

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.i"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/Documents/msas_demo/examples/msas_demo/driver/wishbone_bus.cpp > CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.i

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.s"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/Documents/msas_demo/examples/msas_demo/driver/wishbone_bus.cpp -o CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.s

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.requires:
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.provides: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.requires
	$(MAKE) -f examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.provides.build
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.provides

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.provides.build: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o: ../examples/msas_demo/driver/servo.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/Documents/msas_demo/build/CMakeFiles $(CMAKE_PROGRESS_4)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/matrix_creator_hal.dir/servo.cpp.o -c /home/pi/Documents/msas_demo/examples/msas_demo/driver/servo.cpp

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/matrix_creator_hal.dir/servo.cpp.i"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/Documents/msas_demo/examples/msas_demo/driver/servo.cpp > CMakeFiles/matrix_creator_hal.dir/servo.cpp.i

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/matrix_creator_hal.dir/servo.cpp.s"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/Documents/msas_demo/examples/msas_demo/driver/servo.cpp -o CMakeFiles/matrix_creator_hal.dir/servo.cpp.s

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.requires:
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.provides: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.requires
	$(MAKE) -f examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.provides.build
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.provides

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.provides.build: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o: ../examples/msas_demo/driver/ultrasonic.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/Documents/msas_demo/build/CMakeFiles $(CMAKE_PROGRESS_5)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o -c /home/pi/Documents/msas_demo/examples/msas_demo/driver/ultrasonic.cpp

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.i"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/Documents/msas_demo/examples/msas_demo/driver/ultrasonic.cpp > CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.i

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.s"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/Documents/msas_demo/examples/msas_demo/driver/ultrasonic.cpp -o CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.s

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.requires:
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.provides: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.requires
	$(MAKE) -f examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.provides.build
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.provides

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.provides.build: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o: ../examples/msas_demo/driver/i2c_basic.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/Documents/msas_demo/build/CMakeFiles $(CMAKE_PROGRESS_6)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o -c /home/pi/Documents/msas_demo/examples/msas_demo/driver/i2c_basic.cpp

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.i"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/Documents/msas_demo/examples/msas_demo/driver/i2c_basic.cpp > CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.i

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.s"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/Documents/msas_demo/examples/msas_demo/driver/i2c_basic.cpp -o CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.s

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.requires:
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.provides: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.requires
	$(MAKE) -f examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.provides.build
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.provides

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.provides.build: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/flags.make
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o: ../examples/msas_demo/driver/keypad.cpp
	$(CMAKE_COMMAND) -E cmake_progress_report /home/pi/Documents/msas_demo/build/CMakeFiles $(CMAKE_PROGRESS_7)
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Building CXX object examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++   $(CXX_DEFINES) $(CXX_FLAGS) -o CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o -c /home/pi/Documents/msas_demo/examples/msas_demo/driver/keypad.cpp

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/matrix_creator_hal.dir/keypad.cpp.i"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -E /home/pi/Documents/msas_demo/examples/msas_demo/driver/keypad.cpp > CMakeFiles/matrix_creator_hal.dir/keypad.cpp.i

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/matrix_creator_hal.dir/keypad.cpp.s"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && /usr/bin/c++  $(CXX_DEFINES) $(CXX_FLAGS) -S /home/pi/Documents/msas_demo/examples/msas_demo/driver/keypad.cpp -o CMakeFiles/matrix_creator_hal.dir/keypad.cpp.s

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.requires:
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.provides: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.requires
	$(MAKE) -f examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.provides.build
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.provides

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.provides.build: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o

# Object files for target matrix_creator_hal
matrix_creator_hal_OBJECTS = \
"CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o" \
"CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o" \
"CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o" \
"CMakeFiles/matrix_creator_hal.dir/servo.cpp.o" \
"CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o" \
"CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o" \
"CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o"

# External object files for target matrix_creator_hal
matrix_creator_hal_EXTERNAL_OBJECTS =

examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build.make
examples/msas_demo/driver/libmatrix_creator_hal.a: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --red --bold "Linking CXX static library libmatrix_creator_hal.a"
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && $(CMAKE_COMMAND) -P CMakeFiles/matrix_creator_hal.dir/cmake_clean_target.cmake
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/matrix_creator_hal.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build: examples/msas_demo/driver/libmatrix_creator_hal.a
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/build

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/matrix_driver.cpp.o.requires
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/everloop.cpp.o.requires
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/wishbone_bus.cpp.o.requires
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/servo.cpp.o.requires
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/ultrasonic.cpp.o.requires
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/i2c_basic.cpp.o.requires
examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires: examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/keypad.cpp.o.requires
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/requires

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/clean:
	cd /home/pi/Documents/msas_demo/build/examples/msas_demo/driver && $(CMAKE_COMMAND) -P CMakeFiles/matrix_creator_hal.dir/cmake_clean.cmake
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/clean

examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/depend:
	cd /home/pi/Documents/msas_demo/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/pi/Documents/msas_demo /home/pi/Documents/msas_demo/examples/msas_demo/driver /home/pi/Documents/msas_demo/build /home/pi/Documents/msas_demo/build/examples/msas_demo/driver /home/pi/Documents/msas_demo/build/examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : examples/msas_demo/driver/CMakeFiles/matrix_creator_hal.dir/depend

