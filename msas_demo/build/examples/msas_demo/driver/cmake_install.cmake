# Install script for directory: /home/pi/Documents/msas_demo/examples/msas_demo/driver

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/pi/Documents/msas_demo/build/examples/msas_demo/driver/libmatrix_creator_hal.a")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/matrix_hal" TYPE FILE FILES
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/creator_memory_map.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/everloop.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/everloop_image.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/matrix_driver.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/wishbone_bus.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/servo.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/ultrasonic_data.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/ultrasonic.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/i2c_basic.h"
    "/home/pi/Documents/msas_demo/examples/msas_demo/driver/keypad.h"
    )
endif()

