#include <unistd.h>
#include <iostream>

#include "everloop_image.h"
#include "everloop.h"
#include "wishbone_bus.h"

namespace hal = matrix_hal;

int main() {
  hal::WishboneBus bus;

  bus.SpiInit();

  hal::Everloop everloop;
  hal::EverloopImage image1d;

  everloop.Setup(&bus);

  unsigned counter = 0;

  while (1) {
    for (hal::LedValue& led : image1d.leds) {
      led.red = 0;
      led.green = 0;
      led.blue = 0;
      led.white = 0;
    }
    image1d.leds[(counter / 2) % 35].red = 20;
    image1d.leds[(counter / 7) % 35].green = 30;
    image1d.leds[(counter / 11) % 35].blue = 30;
    image1d.leds[34 - (counter % 35)].white = 10;

    everloop.Write(&image1d);
    ++counter;
    usleep(20000);
  }

  return 0;
}
