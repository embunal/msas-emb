#include <unistd.h>
#include <string>
#include <iostream>
#include "driver/ultrasonic.h"
#include "driver/creator_memory_map.h"
#include "driver/ultrasonic_data.h"

namespace matrix_hal {

UltrasonicPWM::UltrasonicPWM() {}

bool UltrasonicPWM::Read(UltrasonicData* data) {
  if (!wishbone_) return false;
  
  uint8_t sensor_add_offset = 7;

  wishbone_->SpiRead(kGPIOBaseAddress + sensor_add_offset, (unsigned char*)data, 2);
  
  return true;
}
};  // namespace matrix_hal
