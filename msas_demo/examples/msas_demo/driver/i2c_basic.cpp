#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>                     //Needed for I2C port
#include <fcntl.h>                      //Needed for I2C port
#include <sys/ioctl.h>                  //Needed for I2C port
#include <linux/i2c-dev.h>              //Needed for I2C port
#include "i2c_basic.h"
#include <time.h>

// -------------------------------------------------------------| FUNCIONES  |------------------------------------------------------
char i2c_read(char addr){

        int file_i2c;//p8
        unsigned char buffer[2];
//------- OPEN I2C BUS  -------

        char *filename = (char*)"/dev/i2c-1"; //OPEN THE I2C-BUS
        if ((file_i2c = open(filename, O_RDWR)) < 0)
        {
                //ERROR HANDLING: you can check errno to see what went wrong
                printf("Failed to open the i2c bus");
                return 'a';
        }

        //int addr = 0x57;          //<<<<<The I2C address of the slave
        if (ioctl(file_i2c, I2C_SLAVE, addr) < 0)
        {
                printf("Failed to acquire bus access and/or talk to slave.\n");
                //ERROR HANDLING; you can check errno to see what went wrong
                return 'a';
        }

//----- READ BYTES -----
        //length = 1;                   //<<< Number of bytes to read
        if (read(file_i2c, buffer, 2) != 2)             //read() returns the number of bytes actually read, if it doesn't match then an error occurred (e.g. no respons$
        {
                //ERROR HANDLING: i2c transaction failed
                printf("Failed to read from the i2c bus.\n");
        }
        else
        {
                //printf("Data read: %s\n", buffer);
                return buffer[0];
        }

}


void i2c_write(char data){ 
	int file_i2c;//p8
//------- OPEN I2C BUS  -------
	char wr_buffer[2] = {0,data};
        static char *filename = (char*)"/dev/i2c-1"; //OPEN THE I2C-BUS
        if ((file_i2c = open(filename, O_RDWR)) < 0)
        {
                //ERROR HANDLING: you can check errno to see what went wrong
                printf("Failed to open the i2c bus");
                //return 0;
        }

        int addr = 0x27;          //<<<<<The I2C address of the slave
        if (ioctl(file_i2c, I2C_SLAVE, addr) < 0)
        {
                printf("Failed to acquire bus access and/or talk to slave.\n");
                //ERROR HANDLING; you can check errno to see what went wrong
                //return 0;
        }
        //----- WRITE BYTES -----
        //buffer[0] = 0x01;
        //buffer[1] = 0x02;
        //length = 1;                   //<<< Number of bytes to write
        if (write(file_i2c, wr_buffer, 2) != 2)         //write() returns the number of bytes actually written, if it doesn't match then an error occurred (e.g. no res$
        {
                //ERROR HANDLING: i2c transaction failed 
                printf("Failed to write to the i2c bus.\n");
        }
}

//----------------------------------------------------------------------------------

void writeStringlcd (char *str, char col, char row) {
	char *c = str;
	int count = 0; //para contar string
	int fila;
	fila = row;
	setCursor(col,fila);
	while(*c) {
		writeCharlcd(*c);
		c++;
		noCursor();
		if(*c){
			 count++; //que cuente si detecta un caracter
			 //printf("Cuenta");
		}
		if(count>=20){
			//printf("aumenta");
			fila++;
			setCursor(col,fila);
			count = 0;
		}
		if(fila>3){
			usleep(5000000);//espere un tiempo para mostrar primero la anterior info.
			clearDisplay();//limpiar datos para escribir de nuevo.
			fila = 0;
			setCursor(col,fila);
		}
		
	}
	noCursor();
}

//-----------------------------------------------------------------------------------

void writeDatelcd (char *str) {
        char *c = str;
        int count = 0; //para contar string
        int fila = 0;
        while(*c) {
                writeCharlcd(*c);
                c++;
                if(*c){
                         count++; //que cuente si detecta un caracter
                         //printf("Cuenta");
                }
                if(count>=20){
                        //printf("aumenta");
                        fila++;
                        setCursor(0,fila);
                        count = 0;
                }
        }
        noCursor();
}

//------------------------------------------------------------------------------------
void writeCharlcd (char letter) 
{
  char highnib;
  char lownib;
  highnib = letter & 0xF0;
  lownib  = letter & 0x0F;

     i2c_write(highnib|0b00001001);
     i2c_write(highnib|0b00001101);
     i2c_write(highnib|0b00001001);  

     i2c_write((lownib<<4)|0b00001001);
     i2c_write((lownib<<4)|0b00001101);
     i2c_write((lownib<<4)|0b00001001);
}
//------------------------------------------------------------------------------------

void writeCommandlcd (char command) 
{
  char highnib;
  char lownib;
  highnib = command&0xF0;
  lownib = command&0x0F;

     i2c_write(highnib|0b00001000);
     i2c_write(highnib|0b00001100);
     i2c_write(highnib|0b00001000);  

     i2c_write((lownib<<4)|0b00001000);
     i2c_write((lownib<<4)|0b00001100);
     i2c_write((lownib<<4)|0b00001000);
}

//------------------------------------------------------------------------------------

void clearDisplay() 
{
	writeCommandlcd(0b00000001);
	usleep(5*100);
}

//--------------- INTIALIZTION ALGORITHYM --------------------------------------------

void lcdInit ()
{  //1
   // LCD_I2C CONFIG
   // DB7 DB6 DB5 DB4 CTRST EN RW RS
   usleep(50*1000); //en lugar de msleep, usleep (microsegundos)
   i2c_write(0b00111000); //funcion i2c_write solo recibe data.
   i2c_write(0b00111100); 
   i2c_write(0b00111000); 
   usleep(5*1000);
   
   //2
   i2c_write(0b00111000);
   i2c_write(0b00111100);
   i2c_write(0b00111000);
   usleep(5*1000);
   //3
   i2c_write(0b00111000);
   i2c_write(0b00111100);
   i2c_write(0b00111000);
   usleep(1*1000);
   //5
   i2c_write(0b00101000);
   i2c_write(0b00101100);
   i2c_write(0b00101000);
   usleep(1*1000);
   //6
   i2c_write(0b00101000);
   i2c_write(0b00101100);
   i2c_write(0b00101000);
   usleep(1*1000);
   //7
   i2c_write(0b10001000);
   i2c_write(0b10001100);
   i2c_write(0b10001000);
   usleep(1*1000);
   //8
   i2c_write(0b00001000);
   i2c_write(0b00001100);
   i2c_write(0b00001000);
   usleep(1*1000);
   //9
   i2c_write(0b10001000);
   i2c_write(0b10001100);
   i2c_write(0b10001000);
   usleep(1*1000);
   //10
   i2c_write(0b00001000);
   i2c_write(0b00001100);
   i2c_write(0b00001000);
   usleep(2*1000);
   //11
   i2c_write(0b11111000);
   i2c_write(0b11111100);
   i2c_write(0b11111000);
   usleep(1*1000);
   //12
   i2c_write(0b00001000);
   i2c_write(0b00001100);
   i2c_write(0b00001000);
   usleep(1*1000);
   //13
   i2c_write(0b01101000);
   i2c_write(0b01101100);
   i2c_write(0b01101000);
   usleep(2*1000);

}

void returnHome()
{
   writeCommandlcd(0b00000010);
   usleep(2*1000);
}

//------------- ENTRY MODES ----------------------
// I/D = 1, S=0
void entryModeSet2()
{  
  
   writeCommandlcd(0b00000110);
   usleep(1*1000);
}

// I/D = 1, S=1
void entryModeSet()
{  
   writeCommandlcd(0b00000111);
   usleep(1*1000);
}


// I/D = 0, S=0
void entryModeSet3()
{  
   writeCommandlcd(0b00000100);
   usleep(1*1000);
}

// I/D = 0, S=1
void entryModeSet4()
{  
   writeCommandlcd(0b00000101);
   usleep(1*1000);
}

//------------- LCD DISPLAY FUNCTIONS ------------------

void displayOff()
{  
   writeCommandlcd(0b00001000);
   usleep(1*1000);
}

// D=1, C=1, B=1
void displayOn()
{  

   writeCommandlcd(0b00001111);
   usleep(1*1000);
}

// S/C = 0, R/L = 1
void cursorShiftRight()
{  
   writeCommandlcd(0b00010100);
   usleep(1*1000);
}

// S/C = 0, R/L = 0
void cursorShiftLeft()
{  
   writeCommandlcd(0b00010000);
   usleep(1*1000);
}


// S/C = 1, R/L = 1
void displayShiftRight()
{   

   writeCommandlcd(0b00011100);
   usleep(1*1000);
}

// S/C = 1, R/L = 0
void displayShiftLeft()
{  
   writeCommandlcd(0b00011000);
   usleep(1*1000);
}

// D/L = 0, N = 1, F = 0
//4-Bit mode, 2 lines, 5x8 dots
void functionSet()
{  
   writeCommandlcd(0b00101000);
   usleep(1*1000);
}

// Turns the underline cursor off
void noCursor() {
	writeCommandlcd(0b00001100);
}

//------------- LOCATE CURSOR ---------------------------------
void setCursor(char col,char row){
	char row_offsets[] = {0x00,0x40,0x14,0x54};
	writeCommandlcd(0b10000000|(col+row_offsets[row]));
}

//---------------  SHOW TIME FUNCTIONS ------------------------
char *date(){
	time_t rawtime;
	struct tm *info;
	static char buffer[100];
	time( &rawtime );
	info = localtime( &rawtime );
	strftime(buffer,80,"%x -- %X", info);
	//puts(buffer);
	return(buffer);
}

int hour(){
	time_t now;
    struct tm *tm;

    now = time(0);
    if ((tm = localtime (&now)) == NULL) {
        printf ("Error extracting time stuff\n");
        }

  //  printf (tm->tm_hour);

    return tm->tm_hour;

}

int second(){
	time_t now;
    struct tm *tm;

    now = time(0);
    if ((tm = localtime (&now)) == NULL) {
        printf ("Error extracting time stuff\n");
        }

//    printf (tm->tm_min);

    return tm->tm_sec;
}

int minute(){
        time_t now;
    struct tm *tm;

    now = time(0);
    if ((tm = localtime (&now)) == NULL) {
        printf ("Error extracting time stuff\n");
        }

//    printf (tm->tm_min);

    return tm->tm_min;
}


int weekday(){
	time_t now;
    struct tm *tm;

    now = time(0);
    if ((tm = localtime (&now)) == NULL) {
        printf ("Error extracting time stuff\n");
        }

    //printf ("%s",tm->tm_wday);

    return tm->tm_wday;
}
//------------------ INTERFACE FUCTIONS  -------------------------

//Bienvenida

void Bienvenida(){
	clearDisplay();
	setCursor(5,1);
	writeCharlcd('B');
	usleep(500000);
	writeCharlcd('I');
	usleep(500000);
	writeCharlcd('E');
	usleep(500000);
	writeCharlcd('N');
	usleep(500000);
	writeCharlcd('V');
	usleep(500000);
	writeCharlcd('E');
	usleep(500000);
	writeCharlcd('N');
	usleep(500000);
	writeCharlcd('I');
	usleep(500000);
	writeCharlcd('D');
	usleep(500000);
	writeCharlcd('O');
	usleep(500000);
	writeStringlcd("MSAS demo v3",4,2);
	usleep(3000000);
}
//Idle state
void IdState(){
	clearDisplay();
	setCursor(0,1);
	writeDatelcd(date());
	writeStringlcd("Sistema OK",5,3);
}
//Hora pastilla
void pastilla(){
	clearDisplay();
	setCursor(0,1);
	writeDatelcd(date());
	puts(date());
	writeStringlcd("Es hora de tomar su medicina",0,2);
}
//Usuario identificado
void usrid(int usr){
   if(usr == 1){
	clearDisplay();
	writeStringlcd("David Martinez",0,0);
	writeStringlcd("Pastillas: Tiamina",0,2);
   }else if(usr == 2){
        clearDisplay();
        writeStringlcd("Das Freddy Vanegas",0,0);
        writeStringlcd("Pastillas: Aspirina",0,2);
   }else if(usr == 3){
        clearDisplay();
        writeStringlcd("Fabio Castiblanco",0,0);
        writeStringlcd("Pastillas: Aspirina, Dolex",0,2);
   }else if(usr == 0){
        clearDisplay();
        writeStringlcd("Usuario no registrado",0,0);
   }else{
	clearDisplay();
        writeStringlcd("Usuario no valido",0,0);
   }
}

void programar(){

clearDisplay();
writeStringlcd("Por favor, programe sus 3 alarmas",0,1);
usleep(3000000);

}

void prom_dia(){

clearDisplay();
writeStringlcd("Dia de la semana:",0,0);
writeStringlcd("L-1 M-2 C-3 J-4 V-5 S-6 D-0",0,2);

}

void prom_hora(){

clearDisplay();
writeStringlcd("Hora: en formato 24h",0,1);
}

void prom_min(){

clearDisplay();
writeStringlcd("Minuto: 0-59",3,1);

}


//--------------------------------------------------------------
/*
void displayAddress(uint8_t col, uint8_t row){
	int row_offsets[] = { 0x00, 0x40, 0x14, 0x54 };
	if ( row > 2 ) {
		row = 2-1;    // we count rows starting w/0
	}
        writeCommandlcd (0x80|(col + row_offsets[row]));
} */


