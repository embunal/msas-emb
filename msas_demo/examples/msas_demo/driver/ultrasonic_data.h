#ifndef CPP_DRIVER_ULTRASONIC_DATA_H_
#define CPP_DRIVER_ULTRASONIC_DATA_H_

namespace matrix_hal {

class UltrasonicData {
 public:
  uint16_t distance;
};
};      // namespace matrix_hal
#endif  // CPP_DRIVER_PRESSURE_DATA_H_
