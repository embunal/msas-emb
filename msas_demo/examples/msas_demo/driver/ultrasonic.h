#ifndef CPP_DRIVER_ULTRASONIC_H_
#define CPP_DRIVER_ULTRASONIC_H_

#include <string>
#include "matrix_driver.h"
#include "ultrasonic_data.h"


namespace matrix_hal {

class UltrasonicPWM : public MatrixDriver {
 public:
  UltrasonicPWM();
  bool Read(UltrasonicData* data);
};
};      // namespace matrix_hal
#endif  // CPP_DRIVER_EVERLOOP_H_

