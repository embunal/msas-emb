#ifndef CPP_DRIVER_SERVO_H_
#define CPP_DRIVER_SERVO_H_

#include <string>
#include "matrix_driver.h"



namespace matrix_hal {

const uint16_t neutral_count = 37481;

class ServoPWM : public MatrixDriver {
 public:
  ServoPWM();
  bool Move(int pos, uint8_t servo_sel);
};
};      // namespace matrix_hal
#endif  

