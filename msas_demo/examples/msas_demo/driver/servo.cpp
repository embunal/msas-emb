#include <unistd.h>
#include <string>
#include <iostream>
#include "driver/servo.h"
#include "driver/creator_memory_map.h"
#include <cmath>

namespace matrix_hal {

ServoPWM::ServoPWM() {}

bool ServoPWM::Move(int pos, uint8_t servo_sel) {
  if (!wishbone_) return false;
  uint16_t count;
  
  if((pos > 90) || (pos < -90)){

  count = 0;

} else {
  count = neutral_count + (138*pos);
}
  
  wishbone_->SpiWrite16(kGPIOBaseAddress + servo_sel, count);
  
  return true;
}
};  // namespace matrix_hal
