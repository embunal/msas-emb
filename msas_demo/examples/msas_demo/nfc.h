
#include <phhwConfig.h>
#include <ph_Status.h>
#include <phbalReg.h>
#include <phpalI14443p3a.h>
#include <phpalI14443p4.h>
#include <phpalI14443p3b.h>
#include <phpalI14443p4a.h>
#include <phpalMifare.h>
#include <phalMfc.h>
#include <phacDiscLoop.h>
#include <phKeyStore.h>


static phStatus_t LoadProfile(void);
phStatus_t NfcRdLibInit(void);
char * NfcrdlibEx4_MIFAREClassic(void *pParams);
char * get_nfc_id();
int find_usr(char* id);
