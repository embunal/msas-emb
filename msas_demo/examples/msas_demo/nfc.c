#ifdef DEBUG
#include <stdio.h>
#endif/*
#include <phhwConfig.h>
#include <ph_Status.h>
#include <phbalReg.h>
#include <phpalI14443p3a.h>
#include <phpalI14443p4.h>
#include <phpalI14443p3b.h>
#include <phpalI14443p4a.h>
#include <phpalMifare.h>
#include <phalMfc.h>
#include <phacDiscLoop.h>
#include <phKeyStore.h>
*/
#include "nfc.h"

#ifdef   DEBUG
#include <stdio.h>
#define  DEBUG_FLUSH(x)      
#else
#define  DEBUG_FLUSH(x)
#endif

#define NUMBER_OF_KEYENTRIES        2
#define NUMBER_OF_KEYVERSIONPAIRS   2
#define NUMBER_OF_KUCENTRIES        1

#define DATA_BUFFER_LEN             16 
#define MFC_BLOCK_DATA_SIZE         16 

phbalReg_Stub_DataParams_t         sBalReader;                 

phhalHw_Nfc_Ic_DataParams_t        sHal_Nfc_Ic;                
void                              *pHal;                       
uint8_t                            bHalBufferTx[128];          
uint8_t                            bHalBufferRx[128];          

phpalI14443p3a_Sw_DataParams_t     spalI14443p3a;              
phpalI14443p4a_Sw_DataParams_t     spalI14443p4a;              
phpalI14443p3b_Sw_DataParams_t     spalI14443p3b;              
phpalI14443p4_Sw_DataParams_t      spalI14443p4;               
phpalMifare_Sw_DataParams_t        spalMifare;                 

phacDiscLoop_Sw_DataParams_t       sDiscLoop;                  
phalMfc_Sw_DataParams_t            salMfc;                    

phKeyStore_Sw_DataParams_t         sSwkeyStore;                
phKeyStore_Sw_KeyEntry_t           sKeyEntries[NUMBER_OF_KEYENTRIES];                                  
phKeyStore_Sw_KUCEntry_t           sKUCEntries[NUMBER_OF_KUCENTRIES];                                  
phKeyStore_Sw_KeyVersionPair_t     sKeyVersionPairs[NUMBER_OF_KEYVERSIONPAIRS * NUMBER_OF_KEYENTRIES]; 

uint8_t                            bDataBuffer[DATA_BUFFER_LEN];  

uint8_t                            bSak;                      
uint16_t                           wAtqa;                     


const uint8_t GI[] = { 0x46,0x66,0x6D,0x01,0x01,0x10,0x03,0x02,0x00,0x01,0x04,0x01,0xF1};

static uint8_t    aData[50];

uint8_t Key[6] = {0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU};

uint8_t Original_Key[6] = {0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU};

#ifdef   DEBUG
static void PRINT_BUFF(uint8_t *pBuff, uint8_t size)
{
    uint8_t bBufIdx;

    for(bBufIdx = 0; bBufIdx < size; bBufIdx++)
    {
        DEBUG_PRINTF(" %02X",pBuff[bBufIdx]);
    }
}
#else
#define  PRINT_BUFF(x, y)
#endif


static phStatus_t LoadProfile(void)
{
    phStatus_t status = PH_ERR_SUCCESS;

    sDiscLoop.pPal1443p3aDataParams  = &spalI14443p3a;
	sDiscLoop.pPal1443p3bDataParams  = &spalI14443p3b;
    sDiscLoop.pPal1443p4aDataParams  = &spalI14443p4a;
    sDiscLoop.pPal14443p4DataParams  = &spalI14443p4;
    sDiscLoop.pHalDataParams         = &sHal_Nfc_Ic.sHal;
    sDiscLoop.sTypeATargetInfo.sTypeA_P2P.pGi       = (uint8_t *)GI;
    sDiscLoop.sTypeATargetInfo.sTypeA_P2P.bGiLength = sizeof(GI);
    sDiscLoop.sTypeFTargetInfo.sTypeF_P2P.pGi       = (uint8_t *)GI;
    sDiscLoop.sTypeFTargetInfo.sTypeF_P2P.bGiLength = sizeof(GI);
    sDiscLoop.sTypeATargetInfo.sTypeA_P2P.pAtrRes   = aData;
    sDiscLoop.sTypeFTargetInfo.sTypeF_P2P.pAtrRes   = aData;
    sDiscLoop.sTypeATargetInfo.sTypeA_I3P4.pAts     = aData;
    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_BAIL_OUT, 0x00);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_PAS_POLL_TECH_CFG, PHAC_DISCLOOP_POS_BIT_MASK_A);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_PAS_LIS_TECH_CFG, 0x00);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_ACT_LIS_TECH_CFG, 0x00);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_ACT_POLL_TECH_CFG, 0x00);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_ENABLE_LPCD, PH_OFF);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_COLLISION_PENDING, PH_OFF);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_ANTI_COLL, PH_ON);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_TYPEA_DEVICE_LIMIT, 1);
    CHECK_STATUS(status);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_OPE_MODE, RD_LIB_MODE_NFC);

    status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_BAIL_OUT, PHAC_DISCLOOP_POS_BIT_MASK_A);
    CHECK_SUCCESS(status);

    return status;
}

phStatus_t NfcRdLibInit(void)
{
    phStatus_t status;

    status = phbalReg_Stub_Init(&sBalReader, sizeof(phbalReg_Stub_DataParams_t));
    CHECK_SUCCESS(status);

    status = phOsal_Event_Init();
    CHECK_STATUS(status);

    Set_Interrupt();

#ifdef NXPBUILD__PHHAL_HW_PN5180
    status = phbalReg_SetConfig(
        &sBalReader,
        PHBAL_REG_CONFIG_HAL_HW_TYPE,
        PHBAL_REG_HAL_HW_PN5180);
#endif
#ifdef NXPBUILD__PHHAL_HW_RC523
    status = phbalReg_SetConfig(
        &sBalReader,
        PHBAL_REG_CONFIG_HAL_HW_TYPE,
        PHBAL_REG_HAL_HW_RC523);
#endif
#ifdef NXPBUILD__PHHAL_HW_RC663
    status = phbalReg_SetConfig(
        &sBalReader,
        PHBAL_REG_CONFIG_HAL_HW_TYPE,
        PHBAL_REG_HAL_HW_RC663);
#endif
    CHECK_STATUS(status);

    status = phbalReg_SetPort(
        &sBalReader,
        SPI_CONFIG);
    CHECK_STATUS(status);

    status = phbalReg_OpenPort(&sBalReader);
    CHECK_STATUS(status);

    status = phhalHw_Nfc_IC_Init(
                                &sHal_Nfc_Ic,
                                sizeof(phhalHw_Nfc_Ic_DataParams_t),
                                &sBalReader,
                                0,
                                bHalBufferTx,
                                sizeof(bHalBufferTx),
                                bHalBufferRx,
                                sizeof(bHalBufferRx));
    CHECK_SUCCESS(status);

    sHal_Nfc_Ic.sHal.bBalConnectionType = PHHAL_HW_BAL_CONNECTION_SPI;

	Configure_Device(&sHal_Nfc_Ic);

    pHal = &sHal_Nfc_Ic.sHal;

    status = phpalI14443p3a_Sw_Init(&spalI14443p3a, sizeof(phpalI14443p3a_Sw_DataParams_t), &sHal_Nfc_Ic.sHal);
    CHECK_STATUS(status);

    status = phpalI14443p4a_Sw_Init(&spalI14443p4a, sizeof(phpalI14443p4a_Sw_DataParams_t), &sHal_Nfc_Ic.sHal);
    CHECK_STATUS(status);

    status = phpalI14443p4_Sw_Init(&spalI14443p4, sizeof(phpalI14443p4_Sw_DataParams_t), &sHal_Nfc_Ic.sHal);
    CHECK_STATUS(status);

    status = phpalI14443p3b_Sw_Init(&spalI14443p3b, sizeof(phpalI14443p3b_Sw_DataParams_t), &sHal_Nfc_Ic.sHal);
    CHECK_STATUS(status);

    status = phpalMifare_Sw_Init(&spalMifare, sizeof(phpalMifare_Sw_DataParams_t), &sHal_Nfc_Ic.sHal, NULL);
    CHECK_STATUS(status);

    status = phKeyStore_Sw_Init(
                                &sSwkeyStore,
                                sizeof(phKeyStore_Sw_DataParams_t),
                                &sKeyEntries[0],
                                NUMBER_OF_KEYENTRIES,
                                &sKeyVersionPairs[0],
                                NUMBER_OF_KEYVERSIONPAIRS,
                                &sKUCEntries[0],
                                NUMBER_OF_KUCENTRIES);
    CHECK_SUCCESS(status);

    status = phacDiscLoop_Sw_Init(&sDiscLoop, sizeof(phacDiscLoop_Sw_DataParams_t), &sHal_Nfc_Ic.sHal);
    CHECK_SUCCESS(status);

    LoadProfile();

    status = phKeyStore_FormatKeyEntry(&sSwkeyStore, 1, PH_KEYSTORE_KEY_TYPE_MIFARE);
    CHECK_STATUS(status);

    status = phKeyStore_SetKey(&sSwkeyStore, 1, 0, PH_KEYSTORE_KEY_TYPE_MIFARE, &Key[0], 0);
    CHECK_STATUS(status);

    status = phalMfc_Sw_Init(&salMfc, sizeof(phalMfc_Sw_DataParams_t), &spalMifare, &sSwkeyStore);
    CHECK_STATUS(status);

#if defined NXPBUILD__PHHAL_HW_RC523
    status = phhalHw_Rc523_ReadRegister(&sHal_Nfc_Ic.sHal, PHHAL_HW_RC523_REG_VERSION, &bDataBuffer[0]);
    CHECK_SUCCESS(status);
#endif

    return PH_ERR_SUCCESS;
}


char * NfcrdlibEx4_MIFAREClassic(void *pParams)
{
    phStatus_t  status = 0;
    uint16_t    wTagsDetected = 0;
    uint8_t     bUid[PHAC_DISCLOOP_I3P3A_MAX_UID_LENGTH];
    uint8_t     bUidSize;

    Set_Interrupt();

    status = NfcRdLibInit();
    CHECK_STATUS(status);

    while(1)
    {   
        do
        {
            status = phhalHw_FieldOff(pHal);
            CHECK_STATUS(status);

            status = phacDiscLoop_SetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_NEXT_POLL_STATE, PHAC_DISCLOOP_POLL_STATE_DETECTION);
            CHECK_STATUS(status);

            status = phacDiscLoop_Run(&sDiscLoop, PHAC_DISCLOOP_ENTRY_POINT_POLL);

        }while((status & PH_ERR_MASK) != PHAC_DISCLOOP_DEVICE_ACTIVATED);

        status = phacDiscLoop_GetConfig(&sDiscLoop, PHAC_DISCLOOP_CONFIG_TECH_DETECTED, &wTagsDetected);

        if ((status & PH_ERR_MASK) == PH_ERR_SUCCESS)
        {

            if (PHAC_DISCLOOP_CHECK_ANDMASK(wTagsDetected, PHAC_DISCLOOP_POS_BIT_MASK_A))
            {
                if (0x08 == (sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aSak & 0x08))
                {
                    do
                    {           
                        return sDiscLoop.sTypeATargetInfo.aTypeA_I3P3[0].aUid;
                    }while(1);
                }
            }
        }
    }
}

char * get_nfc_id()
{
    int ret = 0;
    char *id;
    
    char *null_pointer;
    null_pointer = 0x00;
    
    ret = Set_Interface_Link();
    if(ret){
    	return null_pointer;
    }
    
    Reset_reader_device();
    id = NfcrdlibEx4_MIFAREClassic(&sDiscLoop);
    PRINT_BUFF(id,4);
    return id;
}

int find_usr(char* id){
	int user;
	if(id[0] == 27 && id[1] == 33 && id[2] == 62 && id[3] == 64){
        	printf("David \n");
		user = 1;
	}else if(id[0] == 139 && id[1] == 175 && id[2] == 62 && id[3] == 64){
        	printf("Freddy \n");
		user = 2;
	}else if(id[0] == 123 && id[1] == 75 && id[2] == 55 && id[3] == 64){
                printf("Fabio \n");
                user = 3;
        }else { 
		printf("Usuario no registrado \n");
		user = 0;
	}
return user;
}
