/*
Este módulo implementa un demultiplexor para conmutar la señal de CS del SPI 
del Bus de datos implementado en la FPGA y el CS del chip NFC
*/
module spi_cs_mux (
	
	input clk,
	input cs_select_i,
	input muxed_cs,
	output nfc_spi_cs,
	output wb_spi_cs

);

reg wb_cs = 1'b1;
reg nfc_cs = 1'b1;

always @(posedge clk)
begin
if(cs_select_i) nfc_cs = muxed_cs;
else wb_cs = muxed_cs;
end

assign nfc_spi_cs = nfc_cs;
assign wb_spi_cs = wb_cs;

endmodule
