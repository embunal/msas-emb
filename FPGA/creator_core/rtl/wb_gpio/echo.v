
module echo(
	input clk,
	input rst, 
	input echo_sig,
	output trigger_signal,
	output reg [14:0] echo_pulse_width
);

reg [4:0] ticker = 5'b00000;
reg sampler = 1'b0;
reg [14:0] pulse_counter = 15'd0;

//Pulsos a 1 MHz
always @(posedge clk)
begin
	if(rst)
	begin
	sampler <= 1'b0;
	ticker <= 5'd0;
	end
	else if (ticker == 5'd25)
	begin
		sampler <= 1'b1;
		ticker <= 5'd0;
	end
		else
		begin
			sampler <= 1'b0;
			ticker <= ticker + 1;
		end
end

//Contar el ancho del pulso proveniente del pin "echo"
always @(posedge sampler)
begin
	
	if(echo_sig)
	begin
		if(rst)
		pulse_counter <= 15'd0;
		else	
			pulse_counter <= pulse_counter + 1;
	end 
	else
	
		begin
		if(pulse_counter != 15'd0)
			begin
				echo_pulse_width <= pulse_counter;
			end
		pulse_counter <= 15'd0;
	end
end

trigger ultrasonic0 
(
   .clk(clk),
   .rst(resetn),
   .PWM(trigger_signal)
);

endmodule
