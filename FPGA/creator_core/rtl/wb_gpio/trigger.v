`timescale 1ns / 1ps

//_________________________________________________________________________
// Este modulo recibe como entrada la duración en pulsos de reloj del ancho
// de pulso del PWM que se debe enviar al motor. Con este ancho de pulsos,
// construye la señal de PWM y la envía al motor.

module trigger (
		// Intputs
				input clk, 
				input rst, 
		// Outputs
				output PWM
    );

// ********** DEFINITIONS *********** //
// ********************************** //

//    `define FPGA_20MHz   
      `define FPGA_50MHz
//    `define FPGA_100MHz

// *********** PARAMETERS *********** // 
// ********************************** // 	


`ifdef FPGA_20MHz
       parameter   FPGA_FREQ  =  20_000_000; // 20MHz
`endif
`ifdef FPGA_50MHz
       parameter   FPGA_FREQ  =  50_000_000; // 50MHz
`endif
`ifdef FPGA_100MHz
       parameter   FPGA_FREQ  =  100_000_000; // 100MHz
`endif

       parameter PWM_COUNT = 2000000;  // # counts to get PWM period (65 ms)



// ************* CLOCK ADJUSTMENT **************** //
// *********************************************** //

// 50 Hz Clock tick generation: 20ms period
reg [20:0] i = 0;

always @(posedge clk) 
begin	
	if ( rst )
		i <= 21'd0;
	else begin
		if (i < PWM_COUNT)
			i <= i + 1;
		else
			i <= 21'd0;
	end
end

wire f50Hz_tick = (i == 21'd0);  // tick every 20ms

// ******** PULSE WIDTH SELECTION ********** //
// ***************************************** //

reg [31:0] PWM_width_reg = 32'd500; //Para 20 us de tiempo en alto 

// El registro del ancho de pulso siempre está actualizandose con lo que le llegue a la entrada:
//always @(posedge enable) PWM_width_reg <= PWM_width;
				
// ******* FINITE STATE MACHINE ********* //
// ************************************** //

reg finish_pulse = 0;
reg [17:0] j = 0;
reg next_state = 0;  // NOT a phisical reg, remembre!!!: all variable inside always @ structure, must be declared as "reg"
reg state = 0;

always @(posedge clk)
begin
	if ( rst ) 
		state <= 0;
	else
		state <= next_state;
end

always @( * )
begin 

	case (state)
		0: if (f50Hz_tick)
			next_state <= 1;
		else 
			next_state <= 0;
		1: if (finish_pulse)
			next_state <= 0;
		else 
			next_state <= 1;
	endcase
end

always @(posedge clk)
begin
	if (state)
		if (j < PWM_width_reg) begin
			j <= j+1;
			finish_pulse <= 0;
		end else begin
			j <= 0;
			finish_pulse <= 1;
		end
	else begin
		finish_pulse <= 0;
		j <= 0;
	end
end

assign PWM = (state == 1);	


endmodule

