//---------------------------------------------------------------------------
// Wishbone General Pupose IO Component
//
//     0x00	
//     0x10     gpio_in    (read-only)
//     0x14     gpio_out   (read/write)
//     0x18     gpio_oe    (read/write)
//
//---------------------------------------------------------------------------

module wb_gpio (
	input              clk,
	input              reset,
	// Wishbone interface
	input              wb_stb_i,
	input              wb_cyc_i,
	//output             wb_ack_o,
	input              wb_we_i,
	input       [13:0] wb_adr_i,
	input        [1:0] wb_sel_i,
	input       [15:0] wb_dat_i,
	output reg  [15:0] wb_dat_o,
	//
	output             intr,  //used for enabling gpio interrupts
	
	//MODIFICACION PARA EL PROYECTO

	// IO Wires
	input		   echo_pulse,
	//output		   [14:0]echo_width,                              //DEFINE LAS ENTRADAS Y SALIDAS DE LA FPGA
	output             PWM_servo0,
        output             PWM_servo1,
        output             PWM_servo2,
        output             PWM_servo3,
        output             trigger_signal
);

//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------

wire [15:0] gpiocr = 16'b0;

//los pines de entrada/Salida del GPIO son ahora wires:

reg [15:0] pwm0 = 16'd0;
reg [15:0] pwm1 = 16'd0;
reg [15:0] pwm2 = 16'd0;
reg [15:0] pwm3 = 16'd0;
wire [14:0] echo_width;

// Wishbone
//reg  ack;
//assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

always @(posedge clk)
begin
	if (reset) begin
		//ack      <= 1'b0;
		//gpio_out <= 'b0;
	end else begin
		// Handle WISHBONE access
		//ack    <= 1'b0;

		if (wb_rd) begin           // read cycle
			//ack <= 1'b1;


		//DEFINE UN CASE PARA LEER O ESCRIBIR LOS DATOS

			case (wb_adr_i[2:0])
			3'b000: wb_dat_o <= gpiocr;
			3'b111: wb_dat_o <= echo_width;
			default: wb_dat_o <= 16'b0;
			endcase
		end else if (wb_wr) begin // write cycle
			//ack <= 1'b1;

			case (wb_adr_i[2:0])
			3'b001: pwm0 <= wb_dat_i;
			3'b010: pwm1 <= wb_dat_i;
			3'b011: pwm2 <= wb_dat_i;
			3'b100: pwm3 <= wb_dat_i;
			//3'b100: pwm4 <= wb_dat_i;
			default: wb_dat_o <= 16'b0;
			endcase
		end
	end
end

// ************* ACTUAL GPIO FUNCTION **********************
// *********************************************************

// modulo creado: Nombre "interfaz".
//gpio interfaz (  
//        .switches( switches ),
//	.leds    ( leds     ),
//	.in      ( gpio_out),
//	.in2     ( gpio_oe ),
//	.out     ( gpio_in )
//);	

motor servo0(
	.clk(       clk       ),
	.rst(       reset     ),
	.PWM_width(    pwm0   ),  // conteo del ancho de pulso calculado por el procesador
	.PWM(       PWM_servo0)   // SALIDA en PWM hacia el servo 0.
);

motor servo1(
	.clk(       clk       ),
	.rst(       reset     ),
	.PWM_width(   pwm1    ),  // conteo del ancho de pulso calculado por el procesador
	.PWM(       PWM_servo1)   // SALIDA en PWM hacia el servo 1.
);

motor servo2(
	.clk(       clk       ),
	.rst(       reset     ),
	.PWM_width(    pwm2   ),  // conteo del ancho de pulso calculado por el procesador
	.PWM(       PWM_servo2)   // SALIDA en PWM hacia el servo 0.
);

motor servo3(
	.clk(       clk       ),
	.rst(       reset     ),
	.PWM_width(   pwm3    ),  // conteo del ancho de pulso calculado por el procesador
	.PWM(       PWM_servo3)   // SALIDA en PWM hacia el servo 1.
);

echo distance0(
	.clk(	    clk	              ),
	.rst(	    reset             ),
	.echo_sig(  echo_pulse        ),
	.trigger_signal(trigger_signal),
	.echo_pulse_width(echo_width)
);

endmodule
