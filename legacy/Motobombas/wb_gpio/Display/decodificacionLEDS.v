`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:53:24 04/10/2016 
// Design Name: 
// Module Name:    decodificacionLEDS 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module decodificacionLEDS(num, segmentos); 

input [7:0] num;
output reg[6:0] segmentos;
 
always @ (num) 
case (num) 
// abc_defg 
//000015180C140E0D=LOCKED
//1E1715180C140E0D=UNLOCKED
8'h00: segmentos = 7'b000_0001;
//1 
8'h01: segmentos = 7'b100_1111; 
//2
8'h02: segmentos = 7'b001_0010; 
//3
8'h03: segmentos = 7'b000_0110; 
//4
8'h04: segmentos = 7'b100_1100; 
//5
8'h05: segmentos = 7'b010_0100; 
//6
8'h06: segmentos = 7'b010_0000; 
//7
8'h07: segmentos = 7'b000_1111; 
//8
8'h08: segmentos = 7'b000_0000; 
//9
8'h09: segmentos = 7'b000_1100;  
//A
8'h0A: segmentos = 7'b000_1000;
//B
8'h0B: segmentos = 7'b110_0000;
//C
8'h0C: segmentos = 7'b011_0001;
//D
8'h0D: segmentos = 7'b100_0010;
//E
8'h0E: segmentos = 7'b011_0000;
//F
8'h0F: segmentos = 7'b011_1000;
//G
8'h10: segmentos= 7'b010_0000;
//H
8'h11: segmentos= 7'b100_1000;
//I
8'h12: segmentos= 7'b100_1111;
//J
8'h13: segmentos= 7'b111_1111;
//K
8'h14: segmentos= 7'b100_1001;
//L
8'h15: segmentos= 7'b111_0001;
//M
8'h16: segmentos= 7'b111_1111;
//N
8'h17: segmentos= 7'b000_1001;
//O
8'h18: segmentos= 7'b000_0001;
//P
8'h19: segmentos= 7'b001_1000;
//Q
8'h1A: segmentos= 7'b111_1111;
//R
8'h1B: segmentos= 7'b011_1001;
//S
8'h1C: segmentos = 7'b010_0100;
//T
8'h1D: segmentos= 7'b111_0000;
//U
8'h1E: segmentos = 7'b100_0001;
//V
8'h1F: segmentos= 7'b111_1111;
//W
8'h20: segmentos= 7'b111_1111;
//X
8'h21: segmentos= 7'b111_1111;
//Y
8'h22: segmentos= 7'b100_0100;
//Z
8'h23: segmentos= 7'b111_1111;
//Espera
8'h24: segmentos= 7'b111_0111;
//linea central -
8'h25: segmentos=	7'b111_1110; 
// grados °
8'h26: segmentos= 7'b001_1100;

// abc_defg 

default: segmentos=7'b111_1111;  
endcase


endmodule
