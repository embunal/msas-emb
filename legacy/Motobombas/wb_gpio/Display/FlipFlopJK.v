`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:30:15 04/10/2016 
// Design Name: 
// Module Name:    FlipFlopJK 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module FlipFlopJK(entrada,reloj,reset,Q);
input entrada; 
output Q;
reg Q;
input reloj,reset;
initial begin Q=1'b0; end
always @ (negedge reloj or posedge reset)
begin
if(reset==1)begin
Q=1'b0;
end
else
case(entrada) 
1 : Q = ~Q;
endcase
end

endmodule
