`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:04:37 04/10/2016 
// Design Name: 
// Module Name:    displaymodule 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module displaymodule(reloj, reset, numeroInstruccion, valor, display, leds);
//FALTA poner valor

input reloj;
input reset;
input [3:0] numeroInstruccion;
input [31:0] valor;

output [6:0] leds;
output [7:0] display;
 
parameter [31:0] temporizacion=1000000000;

//parameter [31:0] valor=32'h2121210A;

wire [63:0] datosAVisualizar;
wire [7:0] letra;

wire [31:0] variable;

assign datosAVisualizar[31:0]=valor;
assign datosAVisualizar[63:32]=variable;


displayAEscoger escogiendoDisplay(reloj,reset,display); 
escogerMensaje escogerMensaje1(reloj, reset, numeroInstruccion, variable);
letraALetra divisorLetras(datosAVisualizar,reloj,reset,display,letra);
decodificacionLEDS numeroLeds(letra,leds);
endmodule
