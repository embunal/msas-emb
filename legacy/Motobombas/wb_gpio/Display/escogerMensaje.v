`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:35:22 04/10/2016 
// Design Name: 
// Module Name:    escogerMensaje 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module escogerMensaje(reloj, reset, numeroInstruccion, mensaje);

input reloj;
input reset;
input [3:0]numeroInstruccion;

output [31:0] mensaje;
reg [31:0] variable;
assign mensaje=variable;

parameter ESPERA=32'h24242424;
parameter INPUT=32'h25251217;
parameter USER=32'h1E1C0E1B;
parameter SELECT=32'h251C0E15;
parameter TEMP=32'h251D0E25;
parameter HOT=32'h11181D25;
parameter TEPID=32'h1D0E1912;
parameter COLD=32'h0C18150D;
parameter TIME=32'h1D12160E;
parameter HELLO=32'h2525110E;
parameter PROFILE=32'h25191B18;
parameter GOOD=32'h1018180D; 

always @(posedge reloj or posedge reset)
if(reset)
begin
variable=ESPERA;
end
else
case (numeroInstruccion)
4'b0000: variable=ESPERA;
4'b0001: variable=USER;
4'b0010: variable=TEMP;
4'b0110: variable=HOT;
4'b0111: variable=TEPID;
4'b1000: variable=COLD;
4'b1001: variable=TIME;
4'b1010: variable=HELLO;
4'b1011: variable=INPUT;
4'b1100: variable=PROFILE;
4'b1101: variable=SELECT;
4'b1111: variable=GOOD;
default: variable=ESPERA;
endcase
endmodule
