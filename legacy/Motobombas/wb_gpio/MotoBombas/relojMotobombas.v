`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    14:34:56 04/17/2016 
// Design Name: 
// Module Name:    relojMotobombas 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module relojMotobombas(relojentrada,reset,relojsalida);
input relojentrada;
input reset;
output relojsalida;

reg [31:0] relojaux;
//10

assign relojsalida=relojaux[27];

always @(posedge relojentrada)
if(reset)
begin
relojaux <=32'b0;
end
else 
begin
relojaux<=relojaux+1;
end
endmodule
