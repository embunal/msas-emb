`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    23:01:07 11/19/2015 
// Design Name: 
// Module Name:    Register 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Register(
input [7:0]value,
input clk_out,
output reg [7:0] data
	
	);
reg [7:0]Aux;

initial Aux <=8'h17;

always @(posedge clk_out)
begin
		if (value!=8'h17)///DIFERENTE DEL DEFAULT
				begin
				Aux <= value;
				data <= Aux;
				end
		else
				data <= Aux;
end


endmodule
