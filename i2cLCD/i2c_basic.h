#ifndef i2c_basic_h
#define i2c_basic_h

int open_i2c_bus(char addr);
char i2c_read();
void i2c_write(char wr_buffer);
void lcdInit();
void writeCharlcd (char letter);
void clearDisplay();
void writeCommandlcd (char command); 
void writeStringlcd (char *str, char col, char row);
void functionSet();
char *date();
char *minute();
char *hour();
char *weekday();
void writeDatelcd (char *str); 
void setCursor(char col,char row);
void displayOff();
void displayOn();
void noCursor();
void Bienvenida();
void IdState();
void pastilla();
void usrid();

#endif//p8
